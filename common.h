#ifndef SDR_QT_COMMON_H
#define SDR_QT_COMMON_H

#include <QMetaType>
#include <QString>
#include <QVector>
#include <SDR/BASE/common.h>

void init_sdrQtAddons();

QVector<Sample_t> SamplesFromFloat(const QVector<float> & samples);
QVector<Sample_t> SamplesFromFloat_i(const QVector<float> & iqSamples);
QVector<Sample_t> SamplesFromFloat_q(const QVector<float> & iqSamples);
QVector<iqSample_t> iqSamplesFromFloat(const QVector<float> & iqSamples);

template <typename T>
QVector<Sample_t> SamplesFromString(const QString Buffer, T sep);
QVector<Sample_t> SamplesFromString(const QString Buffer);
QString SamplesToString(const QVector<Sample_t> samples);

#endif
