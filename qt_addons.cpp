#include "common.h"
#include <QStringList>

void init_sdrQtAddons()
{
  qRegisterMetaType<Frequency_t>("Frequency_t");
  qRegisterMetaType<Sample_t>("Sample_t");
  qRegisterMetaType<iqSample_t>("iqSample_t");
  qRegisterMetaType<Bit_t>("Bit_t");
  qRegisterMetaType<Size_t>("Size_t");
  qRegisterMetaType<Value_t>("Value_t");
  qRegisterMetaType<ValueFloat_t>("ValueFloat_t");
}

QVector<Sample_t> SamplesFromFloat(const QVector<float> &Samples)
{
  QVector<Sample_t> out(Samples.size());
  for(int i = 0; i < out.size(); ++i)
    out[i] = Samples.data()[i];
  return out;
}

QVector<Sample_t> SamplesFromFloat_i(const QVector<float> &iqSamples)
{
  QVector<Sample_t> out(iqSamples.size()/2);
  for(int i = 0; i < out.size(); ++i)
    out[i] = iqSamples.data()[2*i];
  return out;
}

QVector<Sample_t> SamplesFromFloat_q(const QVector<float> &iqSamples)
{
  QVector<Sample_t> out(iqSamples.size()/2);
  for(int i = 1; i < out.size(); ++i)
    out[i] = iqSamples.data()[2*i];
  return out;
}

QVector<iqSample_t> iqSamplesFromFloat(const QVector<float> &iqSamples)
{
  QVector<iqSample_t> out(iqSamples.size()/2);
  for(int i = 0; i < out.size(); ++i)
  {
    out[i].i = iqSamples.data()[2*i];
    out[i].q = iqSamples.data()[2*i+1];
  }
  return out;
}

static QVector<Sample_t> SamplesFromStringList(const QStringList list)
{
  QVector<Sample_t> out;

  foreach (QString str, list)
  {
    bool ok;
    out.append(str.toDouble(&ok));
    if (!ok) return QVector<Sample_t>();
  }
  return out;
}

template <typename T>
QVector<Sample_t> SamplesFromString(const QString Buffer, T sep)
{
  QStringList list = Buffer.simplified().split(sep);
  list.removeAll(QString(""));
  return SamplesFromStringList(list);
}

QVector<Sample_t> SamplesFromString(const QString Buffer)
{
  return SamplesFromString(Buffer, QRegExp("[ ,;]"));
}

QString SamplesToString(QVector<Sample_t> samples)
{
  QString str;
  QString sep = "";
  foreach (Sample_t sample, samples)
  {
    str += sep + QString::number(sample);
    if (sep.isEmpty()) sep = " ";
  }
  return str;
}
