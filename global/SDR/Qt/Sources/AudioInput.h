#ifndef IO_H
#define IO_H

#ifdef __cplusplus
extern "C" {
#endif

#include <SDR/BASE/Abstract.h>

typedef struct
{
  Frequency_t Fs;
} AudioInputCfg_t;

typedef struct
{
  void * Master;
  samples_sink_t fxn;
} AudioInputCallback_bufready_t;
typedef struct
{
  void * Master;
  samples_sink_iq_t fxn;
} AudioInputCallback_bufready_iq_t;

typedef struct
{
  void * d;
} AudioInput_t;

void init_AudioInput(AudioInput_t * This, AudioInputCfg_t * Cfg);
void release_AudioInput(AudioInput_t * This);
void AudioInput_setCallback_bufready(AudioInput_t * This, AudioInputCallback_bufready_t * Cbk);
void AudioInput_setCallback_bufready_iq(AudioInput_t * This, AudioInputCallback_bufready_iq_t * Cbk);

void AudioInput_DevicesList(AudioInput_t * This);

void AudioInput_start(AudioInput_t * This);
void AudioInput_stop(AudioInput_t * This);

#ifdef __cplusplus
}
#endif

#endif // IO_H
