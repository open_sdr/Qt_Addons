#ifndef TAUDIOINPUT_H
#define TAUDIOINPUT_H

#include <SDR/BASE/common.h>
#include <SDR/BASE/Abstract.h>

#include <QObject>
#include <QAudioInput>

namespace SDR
{

class TAudioInputDevice: public QIODevice
{
  Q_OBJECT

 public:
     TAudioInputDevice() : QIODevice()
     {
       Master = nullptr;
       handler = nullptr;
       handler_iq = nullptr;
     }

     void setFormat(const QAudioFormat & format){m_format = format;}

     void setSink(void * Master, samples_sink_t handler)
     {
       this->Master = Master;
       this->handler = handler;
       isComplex = false;
     }
     void setSink(void * Master, samples_sink_iq_t handler)
     {
       this->Master = Master;
       this->handler_iq = handler;
       isComplex = true;
     }

     void start();
     void stop(){close();}

     qint64 readData(char *data, qint64 maxlen) override;
     qint64 writeData(const char *data, qint64 len) override;

 private:
     QAudioFormat m_format;
     bool isComplex;
     void * Master;
     samples_sink_t handler;
     samples_sink_iq_t handler_iq;

 signals:
     void update();
};


class TAudioInput : public QObject
{
  Q_OBJECT
public:
  explicit TAudioInput(const QAudioFormat &format, QObject *parent = nullptr);
  ~TAudioInput();

  void setFs(Frequency_t Fs);
  void setSink(void * Master, samples_sink_t handler);
  void setSink(void * Master, samples_sink_iq_t handler);

public slots:
  void start();
  void stop();

protected:
  QAudioInput * input;
  TAudioInputDevice inputDevice;

  void setFormat(QAudioFormat format);

  void delete_input();

protected slots:
  void inputStateHandler(QAudio::State state);
};

} // SDR

#endif // TAUDIOINPUT_H
