#ifndef TPLOTPARAMSHELPER_H
#define TPLOTPARAMSHELPER_H

#include <QObject>

#include "SDR/BASE/Solvers/SymbolSync.h"

class TPlotParamsHelper : public QObject
{
  Q_OBJECT
public:
  explicit TPlotParamsHelper(QObject *parent = nullptr);

  static QString SyncWindow(SyncWindow_t * ptr)
  {
    return QString("rangeX:0,%1;SymLinesPos:%2,%3,%4;")
        .arg(SyncWindow_CollectorBuf(ptr)->Size-1)
        .arg(SyncWindow_SymbolPos(ptr))
        .arg(SyncWindow_SymbolSize(ptr))
        .arg(SyncWindow_SymbolPos(ptr));
  }
  static QString SyncWindow(iqSyncWindow_t * ptr)
  {
    return QString("rangeX:0,%1;SymLinesPos:%2,%3,%4;")
        .arg(iqSyncWindow_CollectorBuf(ptr)->Size-1)
        .arg(iqSyncWindow_SymbolPos(ptr))
        .arg(iqSyncWindow_SymbolSize(ptr))
        .arg(iqSyncWindow_SymbolPos(ptr));
  }
  static QString SyncWindow(iqSymbolSync_t * ptr)
  {
    return QString("rangeX:0,%1;SymLinesPos:%2,%3,%4;")
        .arg(iqSyncWindow_CollectorBuf(&ptr->window)->Size-1)
        .arg(iqSyncWindow_SymbolPos(&ptr->window))
        .arg(iqSyncWindow_SymbolSize(&ptr->window))
        .arg(iqSyncWindow_SymbolPos(&ptr->window) + iqTimingErrorDetector_SymbolCenter(&ptr->ted));
  }

signals:

public slots:
};

#endif // TPLOTPARAMSHELPER_H
