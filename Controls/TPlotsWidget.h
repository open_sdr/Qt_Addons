#ifndef TPLOTSWIDGET_H_
#define TPLOTSWIDGET_H_

#include <QTabWidget>
#include <QBoxLayout>
#include <QList>

#include <SDR/Qt_Addons/Plots/TPlot.h>

namespace SDR
{

class TPlotsWidget : public QTabWidget
{
  Q_OBJECT

public:
  explicit TPlotsWidget(QWidget *parent = nullptr, QBoxLayout::Direction dir = QBoxLayout::TopToBottom);
  explicit TPlotsWidget(QStringList &tabNames, QList<QBoxLayout::Direction> &tabDirs, QWidget *parent = nullptr);
  ~TPlotsWidget();

  void setDirection(QBoxLayout::Direction dir){setDirection(currentIndex(), dir);}
  void setDirection(int idx, QBoxLayout::Direction dir);

  void setTabParams(QString name, QBoxLayout::Direction dir = QBoxLayout::TopToBottom){setTabText(currentIndex(), name);((QBoxLayout*)Layout(currentIndex()))->setDirection(dir);}

  int addTab(QString name, QBoxLayout::Direction dir = QBoxLayout::TopToBottom);

  void add(TPlot *plot, int stretch = 0){add(currentIndex(), plot,stretch);}
  void add(int tabIdx, TPlot *plot, int stretch = 0);

  void add(TPlotsWidget *widget, int stretch = 0){add(currentIndex(), widget,stretch);}
  void add(int tabIdx, TPlotsWidget *widget, int stretch = 0);

  void clearData();
  void clear();

  void setPlotMinimumSize(int w, int h);

  void enableZoom(Qt::Orientations);
  void disableZoom();
  void enableDrag(Qt::Orientations);
  void disableDrag();

protected:
  QList<TPlot*> Plots;
  QList<TPlotsWidget*> PlotsWidgets;
  int plotMinW, plotMinH;

  QBoxLayout * createLayout(QBoxLayout::Direction dir = QBoxLayout::TopToBottom);
  QBoxLayout * Layout(int idx){return (QBoxLayout*)widget(idx)->layout();}
};

} // SDR

#endif // TPLOTSWIDGET_H_
