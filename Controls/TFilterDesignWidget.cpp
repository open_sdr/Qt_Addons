#include <QDebug>

#include "TFilterDesignWidget.h"
#include "ui_TFilterDesignWidget.h"

#include <sdr_math.h>
#include <liquid/liquid.h>

using namespace SDR;

TFilterDesignWidget::TFilterDesignWidget(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::TFilterDesignWidget)
{ 
  ui->setupUi(this);
  setWindowTitle("SDR Filter Design");

  ui_init_values();
  ui_reset_indexes();

  connect(ui->designButton, SIGNAL(clicked()), this, SLOT(design()));

  connect(ui->type, SIGNAL(currentIndexChanged(int)), this, SLOT(ui_refresh_type()));
  connect(ui->firType, SIGNAL(currentIndexChanged(int)), this, SLOT(ui_refresh_fir_type()));
  connect(ui->firWindowType, SIGNAL(currentIndexChanged(int)), this, SLOT(ui_refresh_fir_window_type()));

  connect(ui->freqUnits, SIGNAL(currentIndexChanged(int)), this, SLOT(ui_refresh_freq_units()));
  connect(ui->magUnits, SIGNAL(currentIndexChanged(int)), this, SLOT(ui_refresh_mag_units()));
}

void TFilterDesignWidget::ui_init_values()
{
  ui->type->clear();
  ui->type->addItem("FIR", FIR);

  ui->firType->clear();
  ui->firType->addItem("Raw Coeffs", RawCoeffs);
  ui->firType->addItem("Window", Window);
  ui->firType->addItem("Parks-McClellan", ParksMcClellan);

  ui->firWindowType->clear();
  ui->firWindowType->addItem("Kaiser", Kaiser);

  ui->freqUnits->clear();
  ui->freqUnits->addItem("norm[0;0.5]", Norm);
  ui->freqUnits->addItem("Hz",Hz);
  ui->freqUnits->addItem("kHz",kHz);
  ui->freqUnits->addItem("Mhz",MHz);
  ui->freqUnits->addItem("GHz",GHz);

  ui->magUnits->clear();
  ui->magUnits->addItem("dB",dB);
  ui->magUnits->addItem("Linear",Linear);
}

void TFilterDesignWidget::ui_reset_indexes()
{
  ui->Tabs->setCurrentIndex(0);

  ui->type->setCurrentIndex(0);
  ui->firType->setCurrentIndex(ui->firType->findData(defaultFirTypeId));
  ui->firWindowType->setCurrentIndex(0);
  ui_refresh_type();

  ui->freqUnits->setCurrentIndex(1);
  oldfUnits = currentFreqUnits();
  if (oldfUnits == Norm)
    fUnitsBeforeNorm = Hz;
  ui_refresh_freq_units();

  ui->magUnits->setCurrentIndex(0);
  oldmUnits = currentMagUnits();
  ui_refresh_mag_units();
}

void TFilterDesignWidget::ui_refresh_type()
{
  QWidget * w = nullptr;
  switch(ui->type->currentData().toInt())
  {
  case FIR:
    w = ui->firTypeModes;
    ui_refresh_fir_type();
    break;
  default: return;
  }
  ui->SpecificTypeModes->setCurrentWidget(w);
}

void TFilterDesignWidget::ui_refresh_fir_type()
{
  int curType = ui->firType->currentData().toInt();

  bool isDesign = true, isFc = false, isUniParams = false;
  QWidget * w = nullptr, * wParams = nullptr;
  switch(curType)
  {
  case RawCoeffs:
    isDesign = false;
    w = ui->firEmptyModes;
    wParams = ui->emptyParams;
    break;
  case Window:
    isFc = true;
    w = ui->firWindowModes;
    ui_refresh_fir_window_type();
    break;
  case ParksMcClellan:
    w = ui->firParksMcClellanModes;
    wParams = ui->firPMCParams;
    isUniParams = true;
    ui_refresh_fir_pmc_uniparams();
    break;
  default: return;
  }
  ui->SpecificFirTypeModes->setCurrentWidget(w);
  if (wParams)
    ui->SpecificParams->setCurrentWidget(wParams);
  ui->groupBox_SpecificParams->setVisible(ui->SpecificParams->currentWidget() != ui->emptyParams);

  ui->label_freqCutoff->setVisible(isFc);
  ui->freqCutoff->setVisible(isFc);
  ui->label_magStopBand->setVisible(isFc);
  ui->magStopBand->setVisible(isFc);

  ui->DesignUniParams->setVisible(isUniParams);

  ui->designButton->setEnabled(isDesign);
  ui->DesignParams->setEnabled(isDesign);
}

void TFilterDesignWidget::ui_refresh_fir_window_type()
{
  QWidget * w = nullptr;
  switch(ui->firWindowType->currentData().toInt())
  {
  case Kaiser:
    w = ui->firWindowKaiserParams;
    break;
  default: return;
  }
  ui->SpecificParams->setCurrentWidget(w);
}

void TFilterDesignWidget::ui_refresh_fir_pmc_uniparams()
{
  ui->DesignUniParams->clear();

  ui->DesignUniParams->setColumnCount(pmcColumnCount);
  QStringList hLabels;
  for (int column = 0; column < pmcColumnCount; ++column)
    hLabels.append(pmcUniParamsColumnHeader((pmcUniParamsColumnId)column));
  ui->DesignUniParams->setHorizontalHeaderLabels(hLabels);

  ui->DesignUniParams->setRowCount(ui->firPMCNumBands->value());
  for (int row = 0; row < ui->DesignUniParams->rowCount(); ++row)
    ui_init_pmc_row(row);
}

void TFilterDesignWidget::ui_refresh_freq_units()
{
  freqUnits curUnits = currentFreqUnits();
  bool isNorm = curUnits == Norm;
  ui->label_freqSampl->setVisible(!isNorm);
  ui->freqSampl->setVisible(!isNorm);

  double min,max,step;
  int decimals;
  QString suff;
  switch (curUnits)
  {
  case Norm:
    min = 0.0;
    max = 0.5;
    step = 0.01;
    suff = "";
    decimals = 11;
    break;

  case Hz:
    min = 0.0;
    max = 99999999999;
    step = 1.0;
    suff = " Hz";
    decimals = 0;
    break;

  case kHz:
    min = 0.0;
    max = 99999999.999;
    step = 1.0;
    suff = " kHz";
    decimals = 3;
    break;

  case MHz:
    min = 0.0;
    max = 9999.9999999;
    step = 1.0;
    suff = " MHz";
    decimals = 6;
    break;

  case GHz:
    min = 0.0;
    max = 99.999999999;
    step = 1.0;
    suff = " GHz";
    decimals = 9;
    break;

  default: return;
  }

  if (!isNorm)
    ui_refresh_freq_spinbox(ui->freqSampl,min,max,step,decimals,suff,(oldfUnits == Norm ? fUnitsBeforeNorm : oldfUnits));

  switch(ui->firType->currentData().toInt())
  {
  case RawCoeffs:
    break;
  case Window:
    ui_refresh_freq_spinbox(ui->freqCutoff,min,max,step,decimals,suff,oldfUnits);
    break;
  case ParksMcClellan:
    ui_refresh_pmc_freqs(suff,oldfUnits);
    break;
  default:
    break;
  }

  if (isNorm && oldfUnits != Norm)
    fUnitsBeforeNorm = oldfUnits;
  oldfUnits = curUnits;
}

void TFilterDesignWidget::ui_refresh_mag_units()
{
  magUnits curUnits = currentMagUnits();
  double min,max,step;
  int decimals;
  QString suff;
  switch(curUnits)
  {
  case Linear:
    min = 0.0;
    max = 999999;
    step = 1.0;
    suff = "";
    decimals = 0;
    break;

  case dB:
    min = 0.0;
    max = 999.99;
    step = 1.0;
    suff = " dB";
    decimals = 2;
    break;

  default: return;
  }
  ui_refresh_spinbox(ui->magStopBand, min,max,step,decimals,suff);
  ui_refresh_spinbox(ui->magGain, -max,max,step,decimals,suff);

  oldmUnits = curUnits;
}

void TFilterDesignWidget::ui_refresh_coeffs()
{
  ui->Coeffs->setPlainText(SamplesToString(CoeffsBuf));
}

TFilterDesignWidget::TypeId TFilterDesignWidget::type()
{
  return (TypeId)ui->type->currentData().toInt();
}

void TFilterDesignWidget::setType(TFilterDesignWidget::TypeId id)
{
  ui->type->setCurrentIndex(ui->type->findData(id));
  ui_refresh_type();
}

TFilterDesignWidget::firTypeId TFilterDesignWidget::firType()
{
  return (firTypeId)ui->firType->currentData().toInt();
}

void TFilterDesignWidget::setFirType(TFilterDesignWidget::firTypeId id)
{
  ui->firType->setCurrentIndex(ui->firType->findData(id));
  ui_refresh_fir_type();
}

TFilterDesignWidget::firWindowTypeId TFilterDesignWidget::firWindowType()
{
  return (firWindowTypeId)ui->firWindowType->currentData().toInt();
}

void TFilterDesignWidget::setFirWindowType(TFilterDesignWidget::firWindowTypeId id)
{
  ui->firWindowType->setCurrentIndex(ui->firWindowType->findData(id));
  ui_refresh_fir_window_type();
}

TFilterDesignWidget::~TFilterDesignWidget()
{
  delete ui;
}

void TFilterDesignWidget::setEditable(bool flag)
{
  ui->designButton->setEnabled(flag);
  ui->setCoeffsButton->setEnabled(flag);
}

void TFilterDesignWidget::setCoeffs(const QVector<Sample_t> buf)
{
  CoeffsBuf = buf;
  ui_refresh_coeffs();
}

void TFilterDesignWidget::setCoeffs(const QString text)
{
  CoeffsBuf = SamplesFromString(text);
  ui_refresh_coeffs();
}

void TFilterDesignWidget::setCoeffsCount(const int n)
{
  ui->coeffsCount->setValue(n);
}

void TFilterDesignWidget::setFsEnabled(bool flag)
{
  ui->freqSampl->setEnabled(flag);
}

void TFilterDesignWidget::setFs(double valHz)
{
  ui->freqSampl->setValue(valHz/freq_factor());
}

void TFilterDesignWidget::setFc(double valHz)
{
  ui->freqCutoff->setValue(valHz/freq_factor());
}

void TFilterDesignWidget::setAs(double dB)
{
  ui->magStopBand->setValue(mag_convert(TFilterDesignWidget::dB,dB));
}

void TFilterDesignWidget::setGain(double dB)
{
  ui->magGain->setValue(mag_convert(TFilterDesignWidget::dB,dB));
}

bool TFilterDesignWidget::setBandsPMC(const QVector<float> bands)
{
  if (ui->firType->currentData().toInt() != ParksMcClellan)
    return false;

  ui_set_pcm_num_bands(bands.count()/2);
  for (int row = 0; row < ui->DesignUniParams->rowCount(); ++row)
  {
    ui_set_pmc_cell_value(row, pmcFstart, QString::number(bands[2*row]/freq_factor()));
    ui_set_pmc_cell_value(row, pmcFstop, QString::number(bands[2*row+1]/freq_factor()));
  }
  return true;
}

bool TFilterDesignWidget::setDesiredRespPMC(const QVector<float> des)
{
  if (ui->firType->currentData().toInt() != ParksMcClellan)
    return false;

  ui_set_pcm_num_bands(des.count());
  for (int row = 0; row < ui->DesignUniParams->rowCount(); ++row)
    ui_set_pmc_cell_value(row, pmcDes, QString::number(des[row]));
  return true;
}

double TFilterDesignWidget::freq_factor(freqUnits units)
{
  switch (units)
  {
  default:
    return 1.0;
  case kHz:
    return 1000.0;
  case MHz:
    return 1000000.0;
  case GHz:
    return 1000000000.0;
  }
}

double TFilterDesignWidget::freq_factor()
{
  return freq_factor(currentFreqUnits());
}

double TFilterDesignWidget::freq_convert(freqUnits inUnits, double freq, freqUnits outUnits)
{
  double res;
  switch (inUnits)
  {
  case Norm:
    switch(outUnits)
    {
    case Norm:
      res = freq;
      break;
    default:
      res = freq*ui->freqSampl->value();
      break;
    }
    break;
  default:
    switch(outUnits)
    {
    case Norm:
      res = freq/ui->freqSampl->value();
      break;
    default:
      res = freq*freq_factor(inUnits)/freq_factor(outUnits);
      break;
    }
    break;
  }
  return res;
}

double TFilterDesignWidget::freq_convert(freqUnits inUnits, double freq)
{
  return freq_convert(inUnits, freq, currentFreqUnits());
}

double TFilterDesignWidget::freq_cutoff(freqUnits outUnits)
{
  return freq_convert(currentFreqUnits(),ui->freqCutoff->value(),outUnits);
}

double TFilterDesignWidget::mag_convert(magUnits inUnits, double mag, magUnits outUnits)
{
  switch (inUnits)
  {
  case Linear:
    switch (outUnits)
    {
    default:
    case Linear:
      break;
    case dB:
      return to_dB_1(mag);
    }
  case dB:
    switch (outUnits)
    {
    case Linear:
      return from_dB_1(mag);

    default:
    case dB:
      break;
    }
  }
  return mag;
}

double TFilterDesignWidget::mag_convert(magUnits inUnits, double mag)
{
  return mag_convert(inUnits,mag,currentMagUnits());
}

double TFilterDesignWidget::mag_stopband(magUnits outUnits)
{
  return mag_convert(currentMagUnits(), ui->magStopBand->value(), outUnits);
}

double TFilterDesignWidget::mag_gain(TFilterDesignWidget::magUnits outUnits)
{
  return mag_convert(currentMagUnits(), ui->magGain->value(), outUnits);
}

void TFilterDesignWidget::ui_refresh_spinbox(QDoubleSpinBox *box, double min, double max, double step, int decimals, QString suff)
{
  box->setMinimum(min);
  box->setMaximum(max);
  box->setSingleStep(step);
  box->setDecimals(decimals);
  box->setSuffix(suff);
}

void TFilterDesignWidget::ui_refresh_freq_spinbox(QDoubleSpinBox *box, double min, double max, double step, int decimals, QString suff, freqUnits oldUnits)
{
  double old = box->value();
  ui_refresh_spinbox(box,min,max,step,decimals,suff);

  box->setValue(freq_convert(oldUnits, old));
}

void TFilterDesignWidget::ui_refresh_mag_spinbox(QDoubleSpinBox *box, double min, double max, double step, int decimals, QString suff)
{
  ui_refresh_spinbox(box,min,max,step,decimals,suff);
}

QString TFilterDesignWidget::ui_pmc_cell_value(int row, TFilterDesignWidget::pmcUniParamsColumnId id)
{
  return ui->DesignUniParams->item(row,id)->text();
}

void TFilterDesignWidget::ui_init_pmc_cell_value(int row, TFilterDesignWidget::pmcUniParamsColumnId id, QString value)
{
  QTableWidgetItem * item = new QTableWidgetItem(value);
  item->setTextAlignment(Qt::AlignVCenter|Qt::AlignCenter);
  ui->DesignUniParams->setItem(row, id, item);
}

void TFilterDesignWidget::ui_set_pmc_cell_value(int row, TFilterDesignWidget::pmcUniParamsColumnId id, QString value)
{
  ui->DesignUniParams->item(row,id)->setText(value);
}

void TFilterDesignWidget::ui_refresh_pmc_freq(int row, TFilterDesignWidget::pmcUniParamsColumnId id, TFilterDesignWidget::freqUnits oldUnits)
{
  float old = ui_pmc_cell_value(row,id).toFloat();
  ui_set_pmc_cell_value(row,id,QString::number(freq_convert(oldUnits, old)));
}

void TFilterDesignWidget::ui_refresh_pmc_freqs(QString suff, TFilterDesignWidget::freqUnits oldUnits)
{
  pmcUniParamsColumnHeader(pmcFstart, suff);
  pmcUniParamsColumnHeader(pmcFstop, suff);
  for (int i = 0; i < ui->DesignUniParams->rowCount();++i)
  {
    ui_refresh_pmc_freq(i,pmcFstart,oldUnits);
    ui_refresh_pmc_freq(i,pmcFstop,oldUnits);
  }
}

void TFilterDesignWidget::ui_init_pmc_row(int row)
{
  ui_init_pmc_cell_value(row, pmcFstart, "0");
  ui_init_pmc_cell_value(row, pmcFstop, "0");
  ui_init_pmc_cell_value(row, pmcDes, "0");
  ui_init_pmc_cell_value(row, pmcW, "1");
  ui_init_pmc_cell_value(row, pmcWType, "0");
}

void TFilterDesignWidget::raw_design()
{
  switch(ui->type->currentData().toInt())
  {
  case FIR:
    design_fir();
    break;
  default: return;
  }
  gain(mag_gain(Linear), CoeffsBuf.data(), CoeffsBuf.size(), CoeffsBuf.data());
  ui_refresh_coeffs();
}

void TFilterDesignWidget::design()
{
  raw_design();
  emit coeffsChanged();
}

QString TFilterDesignWidget::freqUnitsString(freqUnits units)
{
  QString str;
  switch (units)
  {
  case Norm:
    str = "";
    break;
  case Hz:
    str = "Hz";
    break;
  case kHz:
    str = "kHz";
    break;
  case MHz:
    str = "MHz";
    break;
  case GHz:
    str = "GHz";
    break;
  default: break;
  }
  return str;
}

QString TFilterDesignWidget::pmcUniParamsColumnName(TFilterDesignWidget::pmcUniParamsColumnId id)
{
  switch(id)
  {
  default: return QString();
  case pmcFstart:
    return tr("F start");
  case pmcFstop:
    return tr("F stop");
  case pmcDes:
    return tr("response");
  case pmcW:
    return tr("weight");
  case pmcWType:
    return tr("weight type");
  }
}

QString TFilterDesignWidget::pmcUniParamsColumnHeader(TFilterDesignWidget::pmcUniParamsColumnId id)
{
  QString suff;
  switch(id)
  {
  case pmcFstart:
  case pmcFstop:
    suff = ", " + freqUnitsString(currentFreqUnits());
    break;
  default:
    break;
  }
  return pmcUniParamsColumnHeader(id, suff);
}

QString TFilterDesignWidget::pmcUniParamsColumnHeader(TFilterDesignWidget::pmcUniParamsColumnId id, QString suff)
{
  return pmcUniParamsColumnName(id) + suff;
}

TFilterDesignWidget::freqUnits TFilterDesignWidget::currentFreqUnits()
{
  return (freqUnits)ui->freqUnits->currentData().toInt();
}

TFilterDesignWidget::magUnits TFilterDesignWidget::currentMagUnits()
{
  return (magUnits)ui->magUnits->currentData().toInt();
}

void TFilterDesignWidget::design_fir()
{
  switch(ui->firType->currentData().toInt())
  {
  case Window:
    design_fir_window();
    break;
  case ParksMcClellan:
    design_fir_pmc();
    break;
  default: break;
  }
}

void TFilterDesignWidget::design_fir_window()
{
  int coeffsCount = ui->coeffsCount->value();
  CoeffsBuf.resize(coeffsCount);
  switch(ui->firWindowType->currentData().toInt())
  {
  case Kaiser:{
    if (freq_cutoff(Norm) >= 0.5)
    {
      qDebug() << metaObject()->className() << ": design: Kaiser cutoff freq invalid";
      break;
    }
    liquid_firdes_kaiser(coeffsCount, freq_cutoff(Norm), mag_stopband(dB), ui->firWindowKaiserMu->value(),CoeffsBuf.data());
    break;}
  default: break;
  }
}

void TFilterDesignWidget::design_fir_pmc()
{
  int coeffsCount = ui->coeffsCount->value();
  CoeffsBuf.resize(coeffsCount);

  int nBands = ui->firPMCNumBands->value();
  Q_ASSERT(ui->DesignUniParams->rowCount() == nBands);

  QVector<float> bands, des, weight;
  QVector<liquid_firdespm_wtype> wtype;
  bands.resize(2*ui->DesignUniParams->rowCount());
  des.resize(ui->DesignUniParams->rowCount());
  weight.resize(ui->DesignUniParams->rowCount());
  wtype.resize(ui->DesignUniParams->rowCount());
  for (int i = 0; i < ui->DesignUniParams->rowCount();++i)
  {
    bands[2*i] = (freq_convert(currentFreqUnits(),ui_pmc_cell_value(i,pmcFstart).toFloat(),Norm));
    bands[2*i+1] = (freq_convert(currentFreqUnits(),ui_pmc_cell_value(i,pmcFstop).toFloat(),Norm));
    des[i] = (ui_pmc_cell_value(i,pmcDes).toFloat());
    weight[i] = (ui_pmc_cell_value(i,pmcW).toFloat());
    wtype[i] = ((liquid_firdespm_wtype)ui_pmc_cell_value(i,pmcWType).toInt());
  }

  firdespm_run(coeffsCount, ui->DesignUniParams->rowCount(), bands.data(), des.data(), weight.data(), wtype.data(), LIQUID_FIRDESPM_BANDPASS, CoeffsBuf.data());
}

void SDR::TFilterDesignWidget::on_setCoeffsButton_clicked()
{
  CoeffsBuf = SamplesFromString(ui->Coeffs->toPlainText());
  emit coeffsChanged();
}

void TFilterDesignWidget::ui_set_pcm_num_bands(int num)
{
  if (ui->firPMCNumBands->value() != num)
    ui->firPMCNumBands->setValue(num);

  if (num == ui->DesignUniParams->rowCount())
    return;

  int oldRowCount = ui->DesignUniParams->rowCount();
  ui->DesignUniParams->setRowCount(num);

  for (int row = oldRowCount; row < ui->DesignUniParams->rowCount(); ++row)
    ui_init_pmc_row(row);
}

void SDR::TFilterDesignWidget::on_firPMCNumBands_editingFinished()
{
  ui_set_pcm_num_bands(ui->firPMCNumBands->value());
}
