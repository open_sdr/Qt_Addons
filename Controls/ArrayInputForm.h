#ifndef ARRAYINPUTFORM_H
#define ARRAYINPUTFORM_H

#include <QDialog>
#include <QVector>

#include <sdr_basic.h>

namespace Ui {
class ArrayInputForm;
}

namespace SDR
{

class ArrayInputForm : public QDialog
{
  Q_OBJECT

public:
  explicit ArrayInputForm(QWidget *parent = 0);
  virtual ~ArrayInputForm();

  static bool editSamples(QString &vectorStr, QVector<Sample_t> & vector, QString title = "");

private slots:
  void on_OK_clicked();

  void on_Cancel_clicked();

private:
  Ui::ArrayInputForm *ui;
  bool isCanceled;
};

} // SDR

#endif // ARRAYINPUTFORM_H
