#ifndef TSTATUSWIDGETGENERATOR_H
#define TSTATUSWIDGETGENERATOR_H

#include <QObject>
#include "TStatusWidget.h"

#include <QMap>
#include <QJsonObject>

namespace SDR
{

class TStatusWidgetGenerator : public QObject
{
  Q_OBJECT
public:
  explicit TStatusWidgetGenerator(QObject *parent = nullptr);

  static TStatusWidget * create(const char * jsonFormat, QWidget * parent = nullptr);
  static bool read(QJsonObject &json, TStatusWidget * sw);
  static bool read(const char * jsonFormat, TStatusWidget * sw);

signals:

public slots:

protected:
  static bool read_elements(QJsonObject & json, TStatusWidget * sw);
  static void read_element(QJsonObject & json, TStatusWidget * sw);

  static void read_container(QJsonObject & json, TStatusWidget * sw);

  static void read_LedElement(QJsonObject & json, TStatusWidget * sw);
  static void read_FieldElement(QJsonObject & json, TStatusWidget * sw);

  static void read_ElementBase(QJsonObject & json, Status::TElement * el);
  static int read_ElementId(QJsonObject &json);
  static QString read_ElementName(QJsonObject & json);
};

} // SDR

#endif // TSTATUSWIDGETGENERATOR_H
