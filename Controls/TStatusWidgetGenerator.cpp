#include "TStatusWidgetGenerator.h"

#include <SDR/Qt_Addons/Controls/etc/JsonHelpers.h>

using namespace SDR;

TStatusWidgetGenerator::TStatusWidgetGenerator(QObject *parent) : QObject(parent)
{

}

TStatusWidget *TStatusWidgetGenerator::create(const char *jsonFormat, QWidget *parent)
{
  QJsonObject json = jsonParser::parse(jsonFormat);
  if (json.isEmpty()) return nullptr;
  TStatusWidget *sw = new TStatusWidget(jsonParser::direction(json), parent);
  read_elements(json,sw);
  return sw;
}

bool TStatusWidgetGenerator::read(QJsonObject &json, TStatusWidget *sw)
{
  if (json.isEmpty()) return false;
  sw->setDirection(jsonParser::direction(json));
  return read_elements(json,sw);
}

bool TStatusWidgetGenerator::read(const char *jsonFormat, TStatusWidget *sw)
{
  QJsonObject json = jsonParser::parse(jsonFormat);
  return read(json,sw);
}

bool TStatusWidgetGenerator::read_elements(QJsonObject &json, TStatusWidget *sw)
{
  if (!(json.contains("elements") && json["elements"].isArray())) return false;
  QJsonArray jsonElems = json["elements"].toArray();
  for (int i = 0; i < jsonElems.size(); ++i)
  {
    QJsonObject obj = jsonElems[i].toObject();
    read_element(obj, sw);
  }
  sw->appendStretchToCurrentLine();
  return true;
}

void TStatusWidgetGenerator::read_element(QJsonObject &json, TStatusWidget *sw)
{
  if (json.contains("container") && json["container"].isObject())
  {
    QJsonObject obj = json["container"].toObject();
    read_container(obj,sw);
  }
  else if (json.contains("stretch") && json["stretch"].isDouble())
  {
    sw->appendStretchToCurrentLine(json["stretch"].toInt(0));
  }
  else if (json.contains("led") && json["led"].isObject())
  {
    QJsonObject obj = json["led"].toObject();
    read_LedElement(obj, sw);
  }
  else if (json.contains("field") && json["field"].isObject())
  {
    QJsonObject obj = json["field"].toObject();
    read_FieldElement(obj, sw);
  }
}

void TStatusWidgetGenerator::read_container(QJsonObject &json, TStatusWidget *sw)
{
  sw->NewLine(jsonParser::direction(json));
  read_elements(json,sw);
}

void TStatusWidgetGenerator::read_LedElement(QJsonObject &json, TStatusWidget *sw)
{
  Status::TLed * led = new Status::TLed(read_ElementId(json), read_ElementName(json), jsonParser::direction(json));

  read_ElementBase(json, led);

  sw->appendToCurrentLine(led);
}

void TStatusWidgetGenerator::read_FieldElement(QJsonObject &json, TStatusWidget *sw)
{
  Status::TField * field = new Status::TField(read_ElementId(json), read_ElementName(json), jsonParser::direction(json));

  read_ElementBase(json, field);

  sw->appendToCurrentLine(field);
}

void TStatusWidgetGenerator::read_ElementBase(QJsonObject &json, Status::TElement *el)
{
}

int TStatusWidgetGenerator::read_ElementId(QJsonObject &json)
{
  if (json.contains("id") && json["id"].isDouble())
    return json["id"].toInt();
  return -1;
}

QString TStatusWidgetGenerator::read_ElementName(QJsonObject &json)
{
  if (json.contains("name") && json["name"].isString())
    return json["name"].toString();
  return QString();
}
