#ifndef TSYNCWINDOWPLOT_H
#define TSYNCWINDOWPLOT_H

#include "TPlot.h"
#include <SDR/BASE/Solvers/syncWindow.h>
#include <SDR/BASE/Solvers/SymbolSync.h>

using namespace SDR;

class TSyncWindowPlot : public TPlot
{
public:
  TSyncWindowPlot(QWidget *parent = 0);
  TSyncWindowPlot(bool appendRawBuf, bool RawBufIsComplex, QWidget *parent = 0);
  TSyncWindowPlot(SyncWindow_t * syncWindow, QWidget *parent = 0);
  TSyncWindowPlot(iqSyncWindow_t * syncWindow, QWidget *parent = 0);
  TSyncWindowPlot(iqSymbolSync_t * SymbolSync, QWidget *parent = 0);

  ~TSyncWindowPlot();

  void setSymLinesPos(Size_t SymStart, Size_t SymSize, Size_t SymGetPos);

  void setSyncWindow(SyncWindow_t * syncWindow);
  void setSyncWindow(iqSyncWindow_t * syncWindow);
  void setSyncWindow(iqSymbolSync_t * SymbolSync);

  void setSymbolGetOffset(Value_t offset);

  void setSymbolsCount(Size_t count);

  void append(Sample_t *samples, Size_t count);
  void append(iqSample_t *samples, Size_t count);
  void update();

  void clearData();

public slots:
  bool setParam(QString & name, QString &value);

private:
  QCPCurve *PlotableRe, *PlotableIm;
  QVector<double> *Keys, *ValuesRe, *ValuesIm;
  QList<QVector<double>> delayRe, delayIm;

  Size_t SymbolsCount;

  bool isRawBuf, RawBufIsComplex;

  SyncWindow_t * SyncWindow;
  iqSyncWindow_t * iqSyncWindow;

  Size_t SymStart, SymStop, SymGetPos;

  QCPItemStraightLine *syncWindow_SymbolStart, *syncWindow_SymbolGetPos, *syncWindow_SymbolStop;

  void init();
  void refresh_lines_pos();

  void autorange(QCPAxis * axis, double scaleFactor);

  void raw_append(Sample_t *samples, Size_t Count);
  void raw_append(iqSample_t *samples, Size_t Count);
};

#endif // TSYNCWINDOWPLOT_H
