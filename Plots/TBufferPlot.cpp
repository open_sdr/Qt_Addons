#include "TBufferPlot.h"
#include "sdr_math.h"

using namespace SDR;

TBufferPlot::TBufferPlot(TBufferPlot::EType type, QWidget *parent) :
  TPlot("Buffer", parent)
{
  Fs = 1;

  PlotableRe = createCurve();
  PlotableIm = createCurve();
  Keys = new QVector<double>;
  ValuesRe = new QVector<double>;
  ValuesIm = new QVector<double>;

  setType(type);

  flag_isAveraging = false;
  AveragingCount = 0;
}

TBufferPlot::~TBufferPlot()
{
  delete Keys;
  delete ValuesRe;
  delete ValuesIm;
}

void TBufferPlot::setType(TBufferPlot::EType type)
{
  clear();
  Type = type;
  switch (type)
  {
  case typeFreqTwoSided:
    setRangeX(-Fs/2,Fs/2);
    PlotableRe->setPen(QPen(QColor(Qt::cyan)));
    break;

  case typeBuffer:
    PlotableRe->setPen(QPen(QColor(Qt::green)));
    break;

  case typeBufferIQ:
    PlotableRe->setPen(QPen(QColor(Qt::green)));
    PlotableIm->setPen(QPen(QColor(Qt::yellow)));
    break;

  default:
    break;
  }
}

void TBufferPlot::setAveragingEnable(bool enable)
{
  flag_isAveraging = enable;
  if (enable)
  {
    Averaging_flag_isFirst = 1;
  }
  else
  {
    averagingReBuf.clear();
    AveragingCount = 0;
  }
}

void TBufferPlot::append(Sample_t *data, Size_t size)
{
  if (!((Type == typeBuffer)||(Type == typeFreqTwoSided)))
    setType(typeBuffer);

  if (flag_isAveraging)
  {
    if (Averaging_flag_isFirst)
    {
      averagingReBuf.resize(size);
      Averaging_flag_isFirst = 0;
      AveragingCounter = 1;
      copy_buffer(data, size, averagingReBuf.data());
    }
    else
    {
      avaraging(data, averagingReBuf.data(), size, averagingReBuf.data());
      if (++AveragingCounter == AveragingCount)
      {
        Averaging_flag_isFirst = 1;
        raw_append(averagingReBuf.data(), size);
      }
      SDR_ASSERT(AveragingCounter <= AveragingCount);
    }
  }
  else
    raw_append(data, size);
}

void TBufferPlot::append(iqSample_t *data, Size_t size)
{
  if(!(Type == typeBufferIQ))
    setType(typeBufferIQ);

  if (flag_isAveraging)
  {
    if (Averaging_flag_isFirst)
    {
      averagingReImBuf.resize(size);
      Averaging_flag_isFirst = 0;
      AveragingCounter = 1;
      copy_buffer_iq(data, size, averagingReImBuf.data());
    }
    else
    {
      avaraging_iq(data, averagingReImBuf.data(), size, averagingReImBuf.data());
      if (++AveragingCounter == AveragingCount)
      {
        Averaging_flag_isFirst = 1;
        raw_append(averagingReImBuf.data(), size);
      }
      SDR_ASSERT(AveragingCounter <= AveragingCount);
    }
  }
  else
    raw_append(data, size);
}

void TBufferPlot::raw_append(Sample_t* data, Size_t size)
{
  SDR_ASSERT((Type == typeBuffer)||(Type == typeFreqTwoSided));

  Keys->resize(size);
  ValuesRe->resize(size);

  switch (Type)
  {
  default:
  case typeBuffer:
    if (!(IsDragEnabled(Qt::Horizontal)||IsZoomEnabled(Qt::Horizontal)))
      setRangeX(0,size-1);
    for (Size_t i = 0; i < size; ++i)
    {
      Keys->data()[i] = i;
      ValuesRe->data()[i] = data[i];
    }
    break;

  case typeFreqTwoSided:{
    double step = (double)Fs/size;
    Size_t I = 0;
    for (Size_t i = size/2; i < size; ++i,++I)
    {
      Keys->data()[I] = (double)i*step - Fs;
      ValuesRe->data()[I] = data[i];
    }
    for (Size_t i = 0; i < size/2; ++i,++I)
    {
      Keys->data()[I] = (double)i*step;
      ValuesRe->data()[I] = data[i];
    }
    break;}
  }
  send_data_to_refresh(PlotableRe, Keys, ValuesRe, true);
}

void TBufferPlot::raw_append(iqSample_t *data, Size_t size)
{
  SDR_ASSERT(Type == typeBufferIQ);
  Keys->resize(size);
  ValuesRe->resize(size);
  ValuesIm->resize(size);
  setRangeX(0,size-1);
  for (Size_t i = 0; i < size; ++i)
  {
    Keys->data()[i] = i;
    ValuesRe->data()[i] = data[i].i;
    ValuesIm->data()[i] = data[i].q;
  }

  send_data_to_refresh(PlotableRe, Keys, ValuesRe, false);
  send_data_to_refresh(PlotableIm, Keys, ValuesIm, true);
}

void TBufferPlot::setScatterStyle(QCPScatterStyle ss)
{
  PlotableRe->setScatterStyle(ss);
  PlotableIm->setScatterStyle(ss);
}

void TBufferPlot::setLineStyle(QCPCurve::LineStyle ls)
{
  PlotableRe->setLineStyle(ls);
  PlotableIm->setLineStyle(ls);
}

bool TBufferPlot::setParam(QString &name, QString &value)
{
  if (TPlot::setParam(name,value)) return true;
  if (name == "Fs")
  {
    Fs = value.toDouble();
    return true;
  }
  else if (name == "type")
  {
    EType type;
    if (value == "real")
      type = typeBuffer;
    else if (value == "complex")
      type = typeBufferIQ;
    else if (value == "freq")
      type = typeFreqTwoSided;
    else
      return false;
    setType(type);
    return true;
  }
  return false;
}

void TBufferPlot::clearData()
{
  Keys->clear();
  switch(Type)
  {
  case typeBufferIQ:
    ValuesIm->clear();
    PlotableIm->data().data()->clear();
  default:
    ValuesRe->clear();
    PlotableRe->data().data()->clear();
    break;
  }
}
