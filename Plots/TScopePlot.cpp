#include "TScopePlot.h"

using namespace SDR;

TScopePlot::TScopePlot(EMode mode, double SampleTime, QWidget *parent) :
  TPlot("Scope", parent)
{
  this->SampleTime = SampleTime;

  Curve[vReal] = createCurve(QPen(QColor(Qt::green)));
  Curve[vImag] = createCurve(QPen(QColor(Qt::yellow)));
  Curve[vBits] = createCurve(QPen(QColor(Qt::blue)));

  Curve[vBits]->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, 7));

  setMode(mode);

  dataCounter = 0;
  Keys = new QVector<double>;
  for (quint8 i = 0; i < ValuesCount; ++i)
    Values[i] = new QVector<double>;

  T = 0;
  RefreshInterval = 50*SampleTime;
  setTimeRange(RefreshInterval);
}

TScopePlot::~TScopePlot()
{
  delete Keys;
  for (quint8 i = 0; i < ValuesCount; ++i)
    delete Values[i];
}

void TScopePlot::setMode(TScopePlot::EMode mode)
{
  switch(mode)
  {
  case mReal:
    Curve[vReal]->setVisible(true);
    Curve[vImag]->setVisible(false);
    Curve[vBits]->setVisible(false);
    break;
  case mComplex:
    Curve[vReal]->setVisible(true);
    Curve[vImag]->setVisible(true);
    Curve[vBits]->setVisible(false);
    break;
  case mBits:
    Curve[vReal]->setVisible(false);
    Curve[vImag]->setVisible(false);
    Curve[vBits]->setVisible(true);
    break;
//  case mAll:
//    Curve[dtReal]->setVisible(true);
//    Curve[dtImag]->setVisible(true);
//    Curve[dtBits]->setVisible(true);
//    break;
  }
  Mode = mode;
}

void TScopePlot::setRefreshTime(double t)
{
  RefreshInterval = t;
}

void TScopePlot::setTimeRange(double t)
{
  TimeInterval = t;
  CurrentMaxT = TimeInterval;
  xAxis->setRange(0,TimeInterval);
}

void TScopePlot::setFullTimeRange()
{
  xAxis->setRange(0,CurrentMaxT);
}

void TScopePlot::clearData()
{
  T = 0;
  CurrentMaxT = TimeInterval;

  dataCounter = 0;
  Keys->clear();
  for (quint8 i = 0; i < ValuesCount; ++i)
    Values[i]->clear();

  for (quint8 i = 0; i < ValuesCount; ++i)
    Curve[i]->data().data()->clear();

  setRangeX(0, TimeInterval);
}

bool TScopePlot::setParam(QString &name, QString &value)
{
  if (TPlot::setParam(name,value)) return true;
  if (name == "Fs")
  {
    setSampleTime(1.0/value.toDouble());
    return true;
  }
  if (name == "refreshTime")
  {
    setRefreshTime(value.toDouble());
    return true;
  }
  if (name == "timeRange")
  {
    setTimeRange(value.toDouble());
    return true;
  }
  return false;
}

void TScopePlot::prepareDataToAppend(Size_t appendSize)
{
  if (!appendSize) return;

  Size_t newSize = dataCounter + appendSize;
  Keys->resize(newSize);
  switch(Mode)
  {
  case mComplex:
    Values[vImag]->resize(newSize);
  case mReal:
    Values[vReal]->resize(newSize);
    break;

  case mBits:
    Values[vBits]->resize(newSize);
    break;
  }
}

bool TScopePlot::incDataCounterAndProc(double SampleTime)
{
  ++dataCounter;
  if (procT(SampleTime))
  {
    procReady();
    return true;
  }
  return false;
}

void TScopePlot::procReady()
{
  Keys->resize(dataCounter);
  switch(Mode)
  {
  case mComplex:
    Values[vImag]->resize(dataCounter);
    send_data_to_refresh(Curve[vImag], Keys, Values[vImag], false);
  case mReal:
    Values[vReal]->resize(dataCounter);
    send_data_to_refresh(Curve[vReal], Keys, Values[vReal], true);
    break;

  case mBits:
    Values[vBits]->resize(dataCounter);
    send_data_to_refresh(Curve[vBits], Keys, Values[vBits], true);
    break;

  default:
    break;
  }
  dataCounter = 0;
}

bool TScopePlot::procT(double SampleTime)
{
  T += SampleTime;

  if ( (RefreshInterval == 0)||(RefreshInterval - fmod(T,RefreshInterval) <= SampleTime) )
  {
    if (isCollectData)
    {
      if (T > CurrentMaxT)
      {
        CurrentMaxT += RefreshInterval;
        setRangeX(CurrentMaxT - TimeInterval, CurrentMaxT);
      }
    }
    else
      T = 0;
    return true;
  }
  return false;
}

void TScopePlot::raw_append(Sample_t sample)
{
  Keys->data()[dataCounter] = T;
  Values[vReal]->data()[dataCounter] = sample;
}

void TScopePlot::raw_append(iqSample_t sample)
{
  Keys->data()[dataCounter] = T;
  Values[vReal]->data()[dataCounter] = sample.i;
  Values[vImag]->data()[dataCounter] = sample.q;
}

void TScopePlot::raw_append(double SampleTime, Bit_t bit)
{
  Keys->data()[dataCounter] = T;
  Values[vBits]->data()[dataCounter] = bit;
  ++dataCounter;
  Keys->data()[dataCounter] = T+SampleTime;
  Values[vBits]->data()[dataCounter] = bit;
}

void TScopePlot::append(double SampleTime, Sample_t sample)
{
  prepareDataToAppend(1);
  raw_append(sample);
  incDataCounterAndProc(SampleTime);
}

void TScopePlot::append(double SampleTime, iqSample_t sample)
{
  prepareDataToAppend(1);
  raw_append(sample);
  incDataCounterAndProc(SampleTime);
}

void TScopePlot::append(double SampleTime, Bit_t bit)
{
  prepareDataToAppend(2);
  raw_append(SampleTime, bit);
  incDataCounterAndProc(SampleTime);
}

void TScopePlot::append(double SampleTime, Sample_t *data, Size_t size)
{
  Q_ASSERT(Mode == mReal);
  prepareDataToAppend(size);
  for (Size_t i = 0; i < size;)
  {
    raw_append(data[i++]);
    if (incDataCounterAndProc(SampleTime))
      prepareDataToAppend(size-i);
  }
}

void TScopePlot::append(double SampleTime, iqSample_t *data, Size_t size)
{
  Q_ASSERT(Mode == mComplex);
  prepareDataToAppend(size);
  for (Size_t i = 0; i < size;)
  {
    raw_append(data[i++]);
    if (incDataCounterAndProc(SampleTime))
      prepareDataToAppend(size-i);
  }
}

void TScopePlot::append(double SampleTime, Bit_t *data, Size_t size)
{
  Q_ASSERT(Mode == mBits);
  prepareDataToAppend(2*size);
  for (Size_t i = 0; i < size;)
  {
    raw_append(data[i++], SampleTime);
    if (incDataCounterAndProc(SampleTime))
      prepareDataToAppend(2*(size-i));
  }
}

void TScopePlot::append(Sample_t sample)
{
  prepareDataToAppend(1);
  raw_append(sample);
  incDataCounterAndProc(SampleTime);
}

void TScopePlot::append(iqSample_t sample)
{
  prepareDataToAppend(1);
  raw_append(sample);
  incDataCounterAndProc(SampleTime);
}

void TScopePlot::append(Bit_t bit)
{
  prepareDataToAppend(2);
  raw_append(SampleTime, bit);
  incDataCounterAndProc(SampleTime);
}

void TScopePlot::append(Sample_t *data, Size_t size)
{
  Q_ASSERT(Mode == mReal);
  prepareDataToAppend(size);
  for (Size_t i = 0; i < size;)
  {
    raw_append(data[i++]);
    if (incDataCounterAndProc(SampleTime))
      prepareDataToAppend(size-i);
  }
}

void TScopePlot::append(iqSample_t *data, Size_t size)
{
  Q_ASSERT(Mode == mComplex);
  prepareDataToAppend(size);
  for (Size_t i = 0; i < size;)
  {
    raw_append(data[i++]);
    if (incDataCounterAndProc(SampleTime))
      prepareDataToAppend(size-i);
  }
}

void TScopePlot::append(Bit_t *data, Size_t size)
{
  Q_ASSERT(Mode == mBits);
  prepareDataToAppend(2*size);
  for (Size_t i = 0; i < size;)
  {
    raw_append(data[i++], SampleTime);
    if (incDataCounterAndProc(SampleTime))
      prepareDataToAppend(2*(size-i));
  }
}
