#include "TSpectrumPlot.h"
#include "sdr_fft.h"
#include "sdr_complex.h"
#include "sdr_math.h"

using namespace SDR;

extern "C"
{
  static void collector_ready(TSpectrumPlot* This, iqSample_t* samples, Size_t count)
  {
    This->append_raw(samples,count);
  }
}

TSpectrumPlot::TSpectrumPlot(Frequency_t Fd, Size_t Nfft, QWidget *parent) :
  TBufferPlot(TBufferPlot::typeFreqTwoSided, parent)
{
  TPlot::setType("Specturm");
//  plottable()->setPen(QPen(QColor(0, 0, 255)));

//  getCustomPlot()->yAxis->setScaleType(QCPAxis::stLogarithmic);
//  getCustomPlot()->yAxis->setTicker(QSharedPointer<QCPAxisTickerLog>(new QCPAxisTickerLog));
//  getCustomPlot()->yAxis->setNumberFormat("eb");
//  getCustomPlot()->yAxis->setNumberPrecision(0);

  iqSamplesCollectorCfg_t cfgCollector;
  cfgCollector.Overlap = 0;
  cfgCollector.Buf.P = 0;
  cfgCollector.Buf.Size = 0;
  init_iqSamplesCollector(&Collector, &cfgCollector);

  iqSamplesCollectorCallback_t callbackCollector;
  callbackCollector.on_ready = (samples_sink_iq_t)collector_ready;
  iqSamplesCollector_setCallback(&Collector, this, &callbackCollector);

  setFreqType(Fd);
  setNfft(Nfft);
}

TSpectrumPlot::~TSpectrumPlot()
{

}


void TSpectrumPlot::setBufsSize(Size_t n)
{
  Buf.resize(n);
  magBuf.resize(n);
}

void TSpectrumPlot::setCollectorSize(Size_t n)
{
  if (n)
  {
    setBufsSize(n);
    iqSamplesCollector_reset(&Collector);
    iqSamplesCollector_setBuf(&Collector, Buf.data(), n);
  }
}

void TSpectrumPlot::append_raw(iqSample_t *samples, Size_t count)
{
  fft_iq(samples, count, samples);
  append_fft(samples, count);
}

void TSpectrumPlot::setNfft(Size_t n)
{
  clearData();

  double nPow = log2(n), nPowBase;
  if ((n == 0)||(modf(nPow,&nPowBase) == 0))
    Nfft = n;
  else
  {
    Nfft = pow(2,round(nPow));
    n = Nfft;
  }

  setCollectorSize(n);
}

void TSpectrumPlot::append(Sample_t *samples, Size_t count)
{
  if (count == 0) return;
  if (Nfft == 0)
  {
    setBufsSize(pow(2,round(log2(count))));
    if (count > Buf.size())
      count = Buf.size();
    iq_mux_i(samples,count,Buf.data());
    iq_set_q(0,count,Buf.data());
    if (count < Buf.size())
      clear_buffer_iq(Buf.data()+count, Buf.size()-count);
    append_raw(Buf.data(), Buf.size());
    return;
  }
  for (Size_t i = 0; i < count; ++i)
  {
    iqSample_t x;
    x.i = samples[i];
    x.q = 0;
    samples_collector_iq_1(&Collector, x);
  }
}

void TSpectrumPlot::append(iqSample_t *samples, Size_t count)
{
  if (count == 0) return;
  if (Nfft == 0)
  {
    setBufsSize(pow(2,round(log2(count))));
    if (count > Buf.size())
      count = Buf.size();
    copy_buffer_iq(samples,count,Buf.data());
    if (count < Buf.size())
      clear_buffer_iq(Buf.data()+count, Buf.size()-count);
    append_raw(Buf.data(), Buf.size());
    return;
  }
  samples_collector_iq(&Collector, samples, count);
}

void TSpectrumPlot::append_fft(iqSample_t* samples, Size_t count)
{
  Q_ASSERT(count <= (Size_t)magBuf.size());

  iq_magnitude(samples, count, magBuf.data());
  to_dB(magBuf.data(), magBuf.size(), magBuf.data());
  TBufferPlot::append(magBuf.data(), count);
}

bool TSpectrumPlot::setParam(QString &name, QString &value)
{
  if (name == "Fs")
  {
    setFs(value.toDouble());
    return true;
  }
  if (name == "fftN")
  {
    setNfft(value.toDouble());
    return true;
  }
  if (name == "type")
    return true;
  return TBufferPlot::setParam(name,value);

}

void TSpectrumPlot::clearData()
{
  iqSamplesCollector_reset(&Collector);
  TBufferPlot::clearData();
}

