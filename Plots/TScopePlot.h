#ifndef TSCOPEPLOT_H_
#define TSCOPEPLOT_H_

#include "TPlot.h"

namespace SDR
{

class TScopePlot : public TPlot
{
  Q_OBJECT
public:
  enum EMode {mReal,mComplex,mBits} Mode;

  explicit TScopePlot(EMode mode, double SampleTime = 1, QWidget *parent = nullptr);
  ~TScopePlot();

  void setMode(EMode mode);
  void setSampleTime(double SampleTime) {this->SampleTime = SampleTime;}

  double getRefreshInterval(){return RefreshInterval;}

  void setRefreshTime(double t);
  void setTimeRange(double t);
  void setFullTimeRange();

  void append(Sample_t sample);
  void append(iqSample_t sample);
  void append(Bit_t bit);

  void append(Sample_t* data, Size_t size);
  void append(iqSample_t *data, Size_t size);
  void append(Bit_t* data, Size_t size);

  void append(double SampleTime, Sample_t sample);
  void append(double SampleTime, iqSample_t sample);
  void append(double SampleTime, Bit_t bit);

  void append(double SampleTime, Sample_t* data, Size_t size);
  void append(double SampleTime, iqSample_t *data, Size_t size);
  void append(double SampleTime, Bit_t* data, Size_t size);

  void clearData();

public slots:
  bool setParam(QString & name, QString &value);

protected:
  enum EValue {vReal,vImag,vBits, ValuesCount};

  QCPCurve *Curve[ValuesCount];
  Size_t dataCounter;
  QVector<double> *Keys, *Values[ValuesCount];
  double SampleTime, RefreshInterval, TimeInterval, T, CurrentMaxT;

  void prepareDataToAppend(Size_t appendSize);
  bool incDataCounterAndProc(double SampleTime);
  bool procT(double SampleTime);
  void procReady();
  void refresh_datatype();

  void raw_append(Sample_t sample);
  void raw_append(iqSample_t sample);
  void raw_append(double SampleTime, Bit_t bit);
};

} // SDR

#endif // TSCOPEPLOT_H_
